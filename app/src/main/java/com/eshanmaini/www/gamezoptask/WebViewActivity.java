package com.eshanmaini.www.gamezoptask;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class WebViewActivity extends AppCompatActivity {

    private WebView mGameView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        Intent intent = getIntent();
        String path = intent.getStringExtra("FILE_PATH");
        Log.d("WebView", path);

        mGameView = (WebView) findViewById(R.id.game_view);
        WebSettings webSettings = mGameView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);

        mGameView.loadUrl("file://"+path);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        mGameView.destroy();
    }
}
