package com.eshanmaini.www.gamezoptask;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> imageUri;
    private ArrayList<String> names;

    public GridAdapter(Context context, ArrayList<String> imageUri, ArrayList<String> names) {
        this.context = context;
        this.imageUri = imageUri;
        this.names = names;
    }

    @Override
    public int getCount() {
        return imageUri.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if(view == null){
            gridView = new View(context);

            gridView = inflater.inflate(R.layout.item, null);
            SimpleDraweeView mGameImage = (SimpleDraweeView) gridView.findViewById(R.id.game_image);
            TextView mGameName = (TextView) gridView.findViewById(R.id.game_name);

            Uri uri = Uri.parse(imageUri.get(i));
            mGameImage.setImageURI(uri);
            mGameName.setText(names.get(i));


        } else {
            gridView = (View) view;
        }

        return gridView;
    }
}
