package com.eshanmaini.www.gamezoptask;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

public class MainActivity extends AppCompatActivity {

    private GridView mGridView;
    private ArrayList<String> imageUri;
    private ArrayList<String> names;
    private ArrayList<String> zipUrl;
    private ArrayList<String> gameId;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fresco.initialize(getApplicationContext());

        setContentView(R.layout.activity_main);

        mGridView = (GridView) findViewById(R.id.grid_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        imageUri = new ArrayList<>();
        names = new ArrayList<>();
        zipUrl = new ArrayList<>();
        gameId = new ArrayList<>();

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mProgressBar.setVisibility(View.VISIBLE);
                //Toast.makeText(MainActivity.this,getFilesDir().toString(),Toast.LENGTH_LONG).show();
                File gameZipFolder = new File(getFilesDir().getAbsolutePath()+"/"+names.get(i)+".zip");
                if(!gameZipFolder.exists()){
                    Log.d("MainActivity", "Downloading File . . . ");
                    new DownloadFile().execute(zipUrl.get(i), names.get(i));
                } else {
                    Log.d("MainActivity", "File already exists");
                    Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
                    intent.putExtra("FILE_PATH", getFilesDir().getAbsolutePath()+"/"+names.get(i)+"/"+gameId.get(i)+"/index.html");
                    mProgressBar.setVisibility(View.GONE);
                    startActivity(intent);
                }

            }
        });

        ManagedChannel channel = ManagedChannelBuilder.forAddress("ben.gamezop.io", 50051).usePlaintext(true).build();
        InfoServiceGrpc.InfoServiceStub stub = InfoServiceGrpc.newStub(channel);

        GamesRequest request = GamesRequest.newBuilder().build();
        stub.getGames(request, new StreamObserver<GamesResponse>() {
            @Override
            public void onNext(GamesResponse value) {
                for (int i = 0; i < value.getGamesCount(); i++){
                    imageUri.add(value.getGames(i).getCover());
                    names.add(value.getGames(i).getName());
                    zipUrl.add(value.getGames(i).getZipUrl());
                    gameId.add(value.getGames(i).getId());
                }

            }

            @Override
            public void onError(Throwable t) {

                Log.d("MainActivity", "Error: " + t.getMessage());
            }

            @Override
            public void onCompleted() {
                Log.d("MainActivity", "Request Complete");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mGridView.setAdapter(new GridAdapter(getApplicationContext(),imageUri,names));
                    }
                });


            }
        });


    }

    class DownloadFile extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            int count;

            try {
                URL url = new URL(strings[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(getFilesDir().getAbsolutePath() + "/"+strings[1]+".zip");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                Log.d("MainActivity", getFilesDir().getAbsolutePath() + "/"+strings[1]+".zip");

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return getFilesDir().getAbsolutePath() + "/"+strings[1];

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("POST EXECUTE", "message"+s);

            try {
                new UnzipUtility().unzip(s+".zip", s);
                File gameDirectory = new File(s);
                File[] files = gameDirectory.listFiles();
                String path = files[0]+"/index.html";
                Log.d("Path to index.html", path);
                Intent i = new Intent(MainActivity.this, WebViewActivity.class);
                i.putExtra("FILE_PATH", path);
                mProgressBar.setVisibility(View.GONE);
                startActivity(i);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



}

