package com.eshanmaini.www.gamezoptask;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.0.1)",
    comments = "Source: gamezop.proto")
public class InfoServiceGrpc {

  private InfoServiceGrpc() {}

  public static final String SERVICE_NAME = "gamezoptask.InfoService";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.eshanmaini.www.gamezoptask.GamesRequest,
      com.eshanmaini.www.gamezoptask.GamesResponse> METHOD_GET_GAMES =
      io.grpc.MethodDescriptor.create(
          io.grpc.MethodDescriptor.MethodType.UNARY,
          generateFullMethodName(
              "gamezoptask.InfoService", "getGames"),
          io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(com.eshanmaini.www.gamezoptask.GamesRequest.getDefaultInstance()),
          io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(com.eshanmaini.www.gamezoptask.GamesResponse.getDefaultInstance()));

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static InfoServiceStub newStub(io.grpc.Channel channel) {
    return new InfoServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static InfoServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new InfoServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary and streaming output calls on the service
   */
  public static InfoServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new InfoServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class InfoServiceImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * method to get game data
     * </pre>
     */
    public void getGames(com.eshanmaini.www.gamezoptask.GamesRequest request,
        io.grpc.stub.StreamObserver<com.eshanmaini.www.gamezoptask.GamesResponse> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_GET_GAMES, responseObserver);
    }

    @java.lang.Override public io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_GET_GAMES,
            asyncUnaryCall(
              new MethodHandlers<
                com.eshanmaini.www.gamezoptask.GamesRequest,
                com.eshanmaini.www.gamezoptask.GamesResponse>(
                  this, METHODID_GET_GAMES)))
          .build();
    }
  }

  /**
   */
  public static final class InfoServiceStub extends io.grpc.stub.AbstractStub<InfoServiceStub> {
    private InfoServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private InfoServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected InfoServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new InfoServiceStub(channel, callOptions);
    }

    /**
     * <pre>
     * method to get game data
     * </pre>
     */
    public void getGames(com.eshanmaini.www.gamezoptask.GamesRequest request,
        io.grpc.stub.StreamObserver<com.eshanmaini.www.gamezoptask.GamesResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_GET_GAMES, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class InfoServiceBlockingStub extends io.grpc.stub.AbstractStub<InfoServiceBlockingStub> {
    private InfoServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private InfoServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected InfoServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new InfoServiceBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * method to get game data
     * </pre>
     */
    public com.eshanmaini.www.gamezoptask.GamesResponse getGames(com.eshanmaini.www.gamezoptask.GamesRequest request) {
      return blockingUnaryCall(
          getChannel(), METHOD_GET_GAMES, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class InfoServiceFutureStub extends io.grpc.stub.AbstractStub<InfoServiceFutureStub> {
    private InfoServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private InfoServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected InfoServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new InfoServiceFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * method to get game data
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.eshanmaini.www.gamezoptask.GamesResponse> getGames(
        com.eshanmaini.www.gamezoptask.GamesRequest request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_GET_GAMES, getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_GAMES = 0;

  private static class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final InfoServiceImplBase serviceImpl;
    private final int methodId;

    public MethodHandlers(InfoServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_GAMES:
          serviceImpl.getGames((com.eshanmaini.www.gamezoptask.GamesRequest) request,
              (io.grpc.stub.StreamObserver<com.eshanmaini.www.gamezoptask.GamesResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    return new io.grpc.ServiceDescriptor(SERVICE_NAME,
        METHOD_GET_GAMES);
  }

}
