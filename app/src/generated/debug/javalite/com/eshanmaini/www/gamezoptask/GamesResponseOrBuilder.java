// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: gamezop.proto

package com.eshanmaini.www.gamezoptask;

public interface GamesResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:gamezoptask.GamesResponse)
    com.google.protobuf.MessageLiteOrBuilder {

  /**
   * <code>repeated .gamezoptask.Game games = 1;</code>
   */
  java.util.List<com.eshanmaini.www.gamezoptask.Game> 
      getGamesList();
  /**
   * <code>repeated .gamezoptask.Game games = 1;</code>
   */
  com.eshanmaini.www.gamezoptask.Game getGames(int index);
  /**
   * <code>repeated .gamezoptask.Game games = 1;</code>
   */
  int getGamesCount();
}
